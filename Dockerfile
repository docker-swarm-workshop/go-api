FROM golang:1.19.5-alpine3.17 as builder

WORKDIR /build
COPY . .

RUN CGO_ENABLED=0 GOOS=linux go build -ldflags="-w -s" -o app .

FROM scratch

WORKDIR /build

COPY --from=builder /build/app /usr/bin/

ENTRYPOINT ["app"]
