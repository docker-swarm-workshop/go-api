package main

import (
	"fmt"
	"log"
	"net/http"
)

func homePage(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Teretulemast!")
}

func pingPage(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "pong")
}

func handleRequests() {
	http.HandleFunc("/", homePage)
	http.HandleFunc("/ping", pingPage)
	log.Fatal(http.ListenAndServe(":8881", nil))
}

func main() {
	handleRequests()
}
