## Container registry kasutamine

### Token'i loomine
https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html

### Registrisse logimine:
    docker login registry.gitlab.com -u kasutaja -p token

### Image'i buildimine:
    docker build -t registry.gitlab.com/docker-swarm-workshop/go-api .

### Image'i saatmine registrisse:
    docker push registry.gitlab.com/docker-swarm-workshop/go-api

### Paigaldamine:
    cd /srv/docker/app

    docker stack deploy --compose-file docker-compose-prod.yml --with-registry-auth go-api

